# Comandos

Por: **Luis M. Chávez**

## cat
Muestra el contenido de un archivo
- ``` cat <archivo>```
## cd
Accede a un directorio.
- ```cd /directorio```
## ls 
Lista los archivos de un directorio.
- ```ls```


## passwd
Cambia la contrasñe del usuario actual.
- ``` passwd <username>```

## ssh
Te permite conectarte a una instancia de la nueve
- ``` ssh name@ip```
- ej: `` ssh root@1.1.0.1``
## systemctl
Te permite ejecutar comandos de control de servicios 
- ``` systemctl <subcomand> <service_name> ```
  * subcomand: start,stop,restart, status, enable,disable.
- ``` systemctl start sshd ```
<br>
Listar los servicios:
- ``` systemctl list-unit-files | grep service ```

## top
Muestra el estado de los procesos en una tabla
- `` top ``
## df -h 
Muestra el estado de los discos
- ```df -h``` 
## du -h
Muestra el espacio ocupado por los archivos
- ```du -h```
## free
Muestra el espacio libre y total
- ```free```
## YUM 
YUM es una herramienta de administración de paquetes de software.
- ```yum -y install <package>```
- ```yum -y update <package> ```
- ```yum -y remove <package>```

## grep 
Busca una cadena en un archivo
- ```grep <texto> ```
# Networking (Redes)
## ifconfig
Muestra la configuración de las interfaces de red
- ``` ifconfig ``
## ping 
Comprueba la disponibilidad de una dirección IP
- ```ping <ip o dirección> ```
## traceroute
Muestra la ruta de una dirección IP
- ```traceroute <ip o dirección> ```
## nslookup
Muestra la configuración de una dirección IP
- ```nslookup <ip o dirección> ```
## netstat
Muestra el estado de las conexiones de red
- ```netstat```
## route
Muestra la configuración de las rutas de red 
- ```route```
## iptables
Muestra la configuración de las reglas de firewall
-```iptables -L```
## nmcli
Muestra la configuración de las redes wifi
- ```nmcli```
## scp 
Copia un archivo a otro usuario
- ```scp <archivo> <ip>:/carpeta```
## ssh
Conecta a una instancia de la nube
- ``` ssh <ip>```
## telnet
Conecta a una instancia de la nube por telnet
-```telnet <ip>```
## wget 
Descarga un archivo de internet 
- ```wget <url>```

